CREATE TABLE dbo.ACCOUNTS (
    ID uniqueidentifier PRIMARY KEY NOT NULL,
    ACCOUNT_NUMBER varchar,
    BALANCE decimal NOT NULL,
    DATE_INS datetime NOT NULL,
    DATE_UPD datetime
);
GO;
CREATE TABLE dbo.ACCOUNTS_HISTORY(
    ID uniqueidentifier PRIMARY KEY NOT NULL,
    ACCOUNT_ID uniqueidentifier NOT NULL,
    DATE_INS datetime NOT NULL ,
    DATE_UPD datetime ,
    AMOUNT decimal NOT NULL,
    CONSTRAINT  FK_A_H_ACCOUNT_ID FOREIGN KEY (ACCOUNT_ID)
    REFERENCES dbo.ACCOUNTS(ID)


);
GO;

