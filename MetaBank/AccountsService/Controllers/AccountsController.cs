﻿using System;
using System.Threading.Tasks;
using AccountsService.Responses;
using AccountsService.Srvcs;
using Microsoft.AspNetCore.Mvc;
using NLog;
using NLog.Web;

namespace AccountsService.Controllers
{
    public class AccountsController : Controller
    {
        private readonly AccountsSrvc _accountsService;
        private readonly ILogger _logger;


        public AccountsController(AccountsSrvc accountService)
        {
            _accountsService = accountService;
            _logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

            _logger.Debug("on complete accounts controller ctor");
        }

        [HttpGet]
        [Route("api/accounts/{accId}/history")]
        public async Task<BaseResponse> GetAccountHistory(string accId)
        {
            try
            {
                var result = await _accountsService.Get(accId);
                if (string.IsNullOrEmpty(result.Result))
                {
                    return result;
                }


                _logger.Error(result.Result);
                return result;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);
                return await Task.FromResult(new BaseResponse(500, e.Message));
            }
        }


        [HttpPost]
        [Route("api/accounts/{accId}/top-up/{amount}")]
        public async Task<BaseResponse> TopUp(string accId, int amount)
        {
            try
            {
                var result = await _accountsService.TopUp(accId, amount);
                if (result.Status != 200)
                {
                    _logger.Error(result.Result);
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);

                return await Task.FromResult(new BaseResponse(500, e.Message));
            }
        }


        [HttpPost]
        [Route("api/accounts/{accId}/withdraw/{amount}")]
        public async Task<BaseResponse> Withdraw(string accId, int amount)
        {
            try
            {
                var result = await _accountsService.Withdraw(accId, amount);
                if (result.Status != 200)
                {
                    _logger.Error(result.Result);
                    return result;
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);


                return await Task.FromResult(new BaseResponse(500, e.Message));
            }
        }


        [HttpPost]
        [Route("api/accounts/{sourceAccId}/transfer/{destinationAccId}/{amount}")]
        public async Task<BaseResponse> Transfer(string sourceAccId, string destinationAccId, int amount)
        {
            try
            {
                var result = await _accountsService.Transfer(sourceAccId, destinationAccId, amount);
                if (result.Status != 200)
                {
                    _logger.Error(result.Result);
                    return result;
                }

                return result;
            }
            catch (Exception e)
            {
                _logger.Error(e.Message);

                return await Task.FromResult(new BaseResponse(500, e.Message));
            }
        }
    }
}