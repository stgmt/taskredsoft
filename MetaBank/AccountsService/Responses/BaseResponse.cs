﻿namespace AccountsService.Responses
{
    public class BaseResponse
    {

        public BaseResponse(int status, string result)
        {
            Status = status;
            Result = result;
        }
        public int Status { get; set; }
        public string Result { get; set; }
    }
}