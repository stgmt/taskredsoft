﻿namespace AccountsService.Responses
{
    public class TransferResponse : BaseResponse
    {
        public TransferResponse(int status, string result):base(status,result)
        {

        }
        public TransferResponse(string sourceBalance, string destinationBalance, int status, string result) : base(status, result)
        {
            SourceBalance = sourceBalance;
            DestinationBalance = destinationBalance;
        }
        
        public string SourceBalance { get; set; }
        public string DestinationBalance { get; set; }
    }
}