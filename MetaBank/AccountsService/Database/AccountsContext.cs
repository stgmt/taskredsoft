﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AccountsService.Database.MappingConfigurations;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace AccountsService.Database
{
    public class AccountsContext : DbContext
    {
        private AccountsContext _dbContext;

        protected AccountsContext DbContext
        {
            get { return _dbContext ?? (_dbContext = new AccountsContext()); }
        }

        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountHistory> AccountHistories { get; set; }

        private Func<DateTime> TimestampProvider { get; } = ()
            => DateTime.UtcNow;

       
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = new CancellationToken())
        {
            TrackChanges();
            return await base.SaveChangesAsync(cancellationToken);
        }

        private void TrackChanges()
        {
            foreach (var entry in ChangeTracker.Entries()
                .Where(e => e.State == EntityState.Added || e.State == EntityState.Modified))
            {
                if (!(entry.Entity is IEntity auditable)) continue;
                if (entry.State == EntityState.Added)
                {
                    auditable.DateIns = TimestampProvider();
                    auditable.DateUpd = TimestampProvider();
                }
                else
                {
                    auditable.DateUpd = TimestampProvider();
                }
            }
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(Environment.CurrentDirectory)
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();

            var config = builder.Build();

            var constr = config.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(constr, sqlServerOptions => sqlServerOptions.CommandTimeout(10));
            base.OnConfiguring(optionsBuilder);

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new AccountsConfiguration());
            modelBuilder.ApplyConfiguration(new AccountHistoryConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}