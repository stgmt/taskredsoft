﻿using System;

namespace AccountsService.Database
{
    public interface IEntity
    {
        public Guid Id { get; set; }
        public DateTime? DateIns { get; set; }
        public DateTime? DateUpd { get; set; }
    }
}