﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountsService.Database
{
    [Table("ACCOUNTS_HISTORY")]
    public class AccountHistory : IEntity
    {
        public Guid Id { get; set; }
        public Guid? AccountId { get; set; }
        public decimal Amount { get; set; }


        public Account Account { get; set; }
       
        public DateTime? DateIns { get; set; }
        public DateTime? DateUpd { get; set; }
    }
}