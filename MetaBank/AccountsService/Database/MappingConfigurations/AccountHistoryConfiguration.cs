﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountsService.Database.MappingConfigurations
{
    public class AccountHistoryConfiguration : IEntityTypeConfiguration<AccountHistory>
    {
        public void Configure(EntityTypeBuilder<AccountHistory> builder)
        {
            builder.HasKey(a => a.Id);

            builder.Property(a => a.Id).HasColumnName("ID");
            builder.Property(a => a.Amount).HasColumnName("AMOUNT");
            builder.Property(a => a.AccountId)
                .HasColumnName("ACCOUNT_ID");
            builder.Property(a => a.DateIns).HasColumnName("DATE_INS");
            builder.Property(a => a.DateUpd).HasColumnName("DATE_UPD");

            builder
                .HasOne(a => a.Account)
                .WithMany(ah => ah.AccountHistorys)
                .HasForeignKey(a => a.AccountId)
                .HasConstraintName("FK_A_H_ACCOUNT_ID");
        }
    }
}