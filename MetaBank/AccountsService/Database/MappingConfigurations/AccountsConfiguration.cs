﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AccountsService.Database.MappingConfigurations
{
    public class AccountsConfiguration : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.HasKey(a => a.Id);

            builder.Property(a => a.Id).HasColumnName("ID");
            builder.Property(a => a.Balance).HasColumnName("BALANCE");
            builder.Property(a => a.AccountNumber)
                .HasColumnName("ACCOUNT_NUMBER");
            builder.Property(a => a.DateIns).HasColumnName("DATE_INS");
            builder.Property(a => a.DateUpd).HasColumnName("DATE_UPD");
        }
    }
}