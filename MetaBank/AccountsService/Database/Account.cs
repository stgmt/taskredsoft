﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountsService.Database
{
    [Table("ACCOUNTS")]
    public class Account : IEntity
    {
        public string AccountNumber { get; set; }
        public decimal Balance { get; set; }


        public List<AccountHistory> AccountHistorys { get; set; }
        public Guid Id { get; set; }

        public DateTime? DateIns { get; set; }
        public DateTime? DateUpd { get; set; }
    }
}