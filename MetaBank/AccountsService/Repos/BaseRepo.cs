﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using AccountsService.Database;
using Microsoft.EntityFrameworkCore;
using NLog;
using NLog.Web;

namespace AccountsService.Repos
{
    public abstract class BaseRepo<T> : AccountsContext, IRepo<T> where T : class, new()
    {
        public ILogger Logger;
       
        private IQueryable<T> AsNoTracking => DbContext.Set<T>().AsNoTracking();

        public async Task<T> Get(object key)
        {
            return await DbContext.Set<T>().FindAsync(key);
        }

        public async Task<T> Get(Expression<Func<T, bool>> predicate)
        {
            return await DbContext.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public IQueryable<T> GetAll()
        {
            return DbContext.Set<T>().AsQueryable();
        }

        public IQueryable<T> GetAll(Expression<Func<T, bool>> predicate)
        {
            return DbContext.Set<T>().Where(predicate);
        }

        public IQueryable<T> GetAllAsNoTracking()
        {
            return AsNoTracking.AsQueryable();
        }

        public IQueryable<T> GetAllAsNoTracking(Expression<Func<T, bool>> predicate)
        {
            return AsNoTracking.Where(predicate);
        }

        public async Task Update(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));
            if (DbContext.Set<T>().Local.All(e => e != entity)) DbContext.Set<T>().Attach(entity);
            DbContext.Entry(entity).State = EntityState.Modified;
            await DbContext.SaveChangesAsync();
        }

        public async Task Delete(T entity)
        {
            if (entity == null) throw new ArgumentNullException(nameof(entity));

            DbContext.Set<T>().Remove(entity);
            await DbContext.SaveChangesAsync();
        }

        public async Task DeleteRange(IEnumerable<T> items)
        {
            if (items == null) throw new ArgumentNullException(nameof(items));

            DbContext.Set<T>().RemoveRange(items);
            await DbContext.SaveChangesAsync();
        }

        public async Task<bool> Exist(Expression<Func<T, bool>> predicate)
        {
            return await AsNoTracking.AnyAsync(predicate);
        }

        public async Task Insert(T entity)
        {
            if (entity != null) 
            {
                await DbContext.AddAsync(entity);
                await DbContext.SaveChangesAsync();
            }
        }
    }
}