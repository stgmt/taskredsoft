﻿using AccountsService.Database;
using NLog.Web;

namespace AccountsService.Repos
{
    public interface IAccountHistorysRepo : IRepo<AccountHistory>
    {
    }

    public class AccountHistorysRepo : BaseRepo<AccountHistory>, IAccountHistorysRepo
    {
        public AccountHistorysRepo()
        {

            Logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();

        }
    }
}