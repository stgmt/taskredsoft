﻿using AccountsService.Database;
using NLog.Web;

namespace AccountsService.Repos
{
    public interface IAccountsRepo : IRepo<Account>
    {
    }

    public class AccountsRepo : BaseRepo<Account>, IAccountsRepo
    {
        public AccountsRepo()
        {
            Logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
        }
    }
}