﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace AccountsService.Repos
{
    public interface IRepo<T> : IDisposable where T : class, new()
    {
        Task Insert(T entity);
        Task<T> Get(object key);

        Task<T> Get(Expression<Func<T, bool>> predicate);

        IQueryable<T> GetAll();

        IQueryable<T> GetAll(Expression<Func<T, bool>> predicate);

        IQueryable<T> GetAllAsNoTracking();

        IQueryable<T> GetAllAsNoTracking(Expression<Func<T, bool>> predicate);

        Task Update(T entity);
        Task Delete(T entity);

        Task DeleteRange(IEnumerable<T> items);

        Task<bool> Exist(Expression<Func<T, bool>> predicate);
    }
}