﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection.Metadata;
using System.Threading.Tasks;
using AccountsService.Controllers;
using AccountsService.Database;
using AccountsService.Repos;
using AccountsService.Responses;

using Newtonsoft.Json;
using NLog;
using NLog.Web;

namespace AccountsService.Srvcs
{
    public class AccountsSrvc
    {
        private readonly AccountHistorysRepo _accountHistorysRepo;
        private readonly AccountsRepo _accountsRepo;
        private readonly ILogger _logger;

        public List<BaseResponse> Errors { get; set; }

        public AccountsSrvc()
        {
            Errors = new List<BaseResponse>();
            _logger = NLogBuilder.ConfigureNLog("nlog.config").GetCurrentClassLogger();
            _accountsRepo = new AccountsRepo();
            _accountHistorysRepo = new AccountHistorysRepo();

            _logger.Debug("NLog injected into AccountsSrvc");
        }


        private List<Guid> ValdiateId(params string[] id)
        {
            var result = new List<Guid>();

            foreach (var item in id)
            {
                var _id = Guid.Empty;
                try
                {
                    _id = Guid.Parse(item);
                    result.Add(_id);
                }
                catch (Exception e)
                {

                    Errors.Add(new BaseResponse(400, $"Ошибка валидации. Неправильный формат Id: {e.Message}"));
                    return result;
                }

            }
            return result;
        }
        private async Task<BaseResponse> ValidateAccount(Account account)
        {

            if (account.Balance < 0)
            {
                return await Task.FromResult(new BaseResponse
                (400, $"Ошибка валидации. Баланс не может отрицательным. balance: {account.Balance}"));
            }

            if (string.IsNullOrEmpty(account.AccountNumber))
            {

                return await Task.FromResult(new BaseResponse
                (400, $"Ошибка валидации. Поле \"Номер аккаунта\" не заполнено."));
            }

            return await Task.FromResult(new BaseResponse(200, "success"));
        }

        private async Task<BaseResponse> ValidateAccountHistory(AccountHistory accountHistory)
        {
            if (accountHistory.AccountId == null || accountHistory.AccountId == Guid.Empty)
            {

                return await Task.FromResult(new BaseResponse
                (400, $"Ошибка валидации. Поле \"AccountId\" не заполнено."));
            }

            var isExist = await _accountsRepo.Exist(x => x.Id == accountHistory.AccountId);
            if (!isExist)
            {

                return await Task.FromResult(new BaseResponse
                (404, $"Ошибка. Cущности с таким \"AccountId\" не найдено. Id: {accountHistory.AccountId}"));
            }
            return await Task.FromResult(new BaseResponse(200, "success"));
        }

        public async Task<BaseResponse> TopUp(string accId, int amount)
        {
            if (amount < 0)
            {
                return await Task.FromResult(new BaseResponse
                (400, $"Ошибка валидации. Поле \"Amount\" должно быть больше нуля. amount: {amount}"));
            }
            var _id = ValdiateId(accId)[0];

            if (Errors.Any()) { return Errors[0]; }

            var isExist = await _accountsRepo.Get(x => x.Id == _id);
            if (isExist == null)
            {
                return await Task.FromResult(new BaseResponse
                (404, $"Cущности с таким \"AccountId\" не найдено. Id: {_id}"));

            }


            var tempBalance = isExist.Balance;
            isExist.Balance = isExist.Balance + amount;

            var isAccValid = await ValidateAccount(isExist);
            if (isAccValid.Status != 200) return new BaseResponse(isAccValid.Status, isAccValid.Result);

            await _accountsRepo.Update(isExist);
            var accHistory = new AccountHistory
            { Amount = amount, AccountId = _id, Id = Guid.NewGuid() };
            var isAccHValid =
                await ValidateAccountHistory(accHistory);
            if (isAccHValid.Status != 200)
            {
                return new BaseResponse(isAccHValid.Status, isAccHValid.Result);
            }

            await _accountHistorysRepo.Insert(accHistory);

            return await Task.FromResult(new BaseResponse
            (200, isExist.Balance.ToString(CultureInfo.InvariantCulture)));
        }

        public async Task<BaseResponse> Withdraw(string accId, int amount)
        {
            if (amount < 0)
            {
                return await Task.FromResult(new BaseResponse(400, $"Ошибка валидации. Поле \"Amount\" должно быть больше нуля. amount: {amount}"));
            }
            var _id = ValdiateId(accId)[0];

            if (Errors.Any()) { return Errors[0]; }

            var isExist = await _accountsRepo.Get(x => x.Id == _id);
            if (isExist == null)
            {
                return await Task.FromResult(new BaseResponse(404, $"Cущности с таким \"AccountId\" не найдено. Id: {_id}"));
            }



            isExist.Balance = isExist.Balance - amount;

            var isValid = await ValidateAccount(isExist);
            if (isValid.Status != 200) return new BaseResponse(isValid.Status, isValid.Result);


            await _accountsRepo.Update(isExist);

            var accHistory = new AccountHistory
            { Amount = amount, AccountId = _id, Id = Guid.NewGuid() };
            var isAccHValid =
                await ValidateAccountHistory(accHistory);

            if (isAccHValid.Status != 200)
            {
                return new BaseResponse(isAccHValid.Status, isAccHValid.Result);
            }

            await _accountHistorysRepo.Insert(accHistory);




            return await Task.FromResult(new BaseResponse
            (200, isExist.Balance.ToString(CultureInfo.InvariantCulture)));
        }

        public async Task<TransferResponse> Transfer(string sourceAccId, string destinationAccId, int amount)
        {
            if (amount < 0)
            {

                return await Task.FromResult(new TransferResponse(400, $"Ошибка валидации. Поле \"Amount\" должно быть больше нуля. amount: {amount}"));
            }

            var _id = ValdiateId(sourceAccId, destinationAccId);

            if (Errors.Any()) { return Errors[0] as TransferResponse; }


            var isSourceExist = await _accountsRepo.Get(x => x.Id == _id[0]);
            var isDestinationExist = await _accountsRepo.Get(x => x.Id == _id[1]);

            if (isDestinationExist == null || isSourceExist == null)
            {

                return await Task.FromResult(new TransferResponse
                (404, $"Cущностей с таким \"AccountId\" не найдено. " +
                           $"sourceId: {sourceAccId} - destinationId: {destinationAccId} "));
            }



            isSourceExist.Balance = isSourceExist.Balance - amount;
            var isSourceValid = await ValidateAccount(isSourceExist);
            if (isSourceValid.Status != 200) return await Task.FromResult(isSourceValid as TransferResponse);

            await _accountsRepo.Update(isSourceExist);
            isDestinationExist.Balance = isDestinationExist.Balance + amount;
            await _accountsRepo.Update(isDestinationExist);





            var accHistorySource = new AccountHistory
            { Amount = amount, AccountId = isSourceExist.Id, Id = Guid.NewGuid() };
            var isAccHValid =
                await ValidateAccountHistory(accHistorySource);

            if (isAccHValid.Status != 200)
            {
                return await Task.FromResult(isAccHValid as TransferResponse);
            }

            await _accountHistorysRepo.Insert(accHistorySource);






            var accHistoryDestination = new AccountHistory
            { Amount = amount, AccountId = isDestinationExist.Id, Id = Guid.NewGuid() };
            var isAccHDValid =
                await ValidateAccountHistory(accHistoryDestination);

            if (isAccHDValid.Status != 200)
            {
                return await Task.FromResult(isAccHDValid as TransferResponse);
            }

            await _accountHistorysRepo.Insert(accHistoryDestination);



            return await Task.FromResult(new TransferResponse(isSourceExist.Balance.ToString(),
                isDestinationExist.Balance.ToString(), 200, "success"));
        }

        public async Task<BaseResponse> Get(string accId = default)
        {
            var _id = Guid.Empty;
            try
            {
                _id = Guid.Parse(accId);
            }
            catch (Exception e)
            {


                return await Task.FromResult(new BaseResponse(400,
                    $"Ошибка валидации. Неправильный формат Id: {e.Message}"));
            }

            if (accId != default)
            {

                var accountHistory =
                    _accountHistorysRepo.GetAll(x => x.AccountId == _id).ToList();

                if (!accountHistory.Any())
                {

                    return await Task.FromResult(new BaseResponse(404, $"Cущности с таким \"AccountId\" не найдено. Id: {accId}"));
                }

                return await Task.FromResult(new BaseResponse
                (200, JsonConvert.SerializeObject(accountHistory)));
            }
            var result =
                    _accountHistorysRepo.GetAll().ToList();
            if (!result.Any())
            {

                return await Task.FromResult(new BaseResponse(400, $"Список пуст"));
            }

            return await Task.FromResult(new BaseResponse
            (200, JsonConvert.SerializeObject(result)));
        }
    }
}